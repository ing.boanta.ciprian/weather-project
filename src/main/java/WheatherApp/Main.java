package WheatherApp;

import Util.HibernateUtil;

import java.io.IOException;

public class Main {

    public static void main(String[] args) throws IOException {

       Menu.showMenu();
        HibernateUtil.shutdown();
    }
}
