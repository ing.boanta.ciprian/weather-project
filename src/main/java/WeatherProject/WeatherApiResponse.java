package WeatherProject;

public class WeatherApiResponse {
    private String name;
    private String country;
    private String region;
    private double lat;
    private double lon;
    private String timezone_id;
    private int temperature;
    private double localtime;
    private double localtime_epoch;
        public WeatherApiResponse(String name, String country, String region, double lat, double lon, String timezone_id,
                                  int temperature, double localtime, double localtime_epoch){
            this.name = name;
            this.country = country;
            this.region = region;
            this.lat = lat;
            this.lon = lon;
            this.timezone_id = timezone_id;
            this.temperature = temperature;
            this.localtime = localtime;
            this.localtime_epoch = localtime_epoch;
        }
}
